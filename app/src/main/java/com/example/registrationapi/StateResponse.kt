package com.example.registrationapi

data class StateResponse(
        val data: List<StateData>,
        val message: String,
        val status: String
)