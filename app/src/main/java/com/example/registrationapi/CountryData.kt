package com.example.registrationapi


data class CountryData(
        val country_id: String,
        val country_name: String
)
