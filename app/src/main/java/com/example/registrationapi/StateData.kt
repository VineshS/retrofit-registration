package com.example.registrationapi

data class StateData(
    val state_id: String,
    val state_name: String
)