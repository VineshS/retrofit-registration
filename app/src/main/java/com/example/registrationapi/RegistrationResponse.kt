package com.example.registrationapi

data class RegistrationResponse(
        val data: RegistrationData,
        val message: String,
        val status: String
)