package com.example.registrationapi

data class CityData(
    val city_id: String,
    val city_name: String
)