package com.example.registrationapi

data class CityResponse(
        val data: List<CityData>,
        val message: String,
        val status: String
)